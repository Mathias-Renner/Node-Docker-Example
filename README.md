# Node-Docker-Example
A simple Node.js example to show Docker workflows

# How to use
  - Install Docker and Docker Compose
  - Run `docker-compose up -d`
  - Develop the app in `src` folder
