FROM node
WORKDIR /app
EXPOSE 3000

COPY src/package.json /app/
RUN npm install

COPY . /app/

CMD node src/app.js
